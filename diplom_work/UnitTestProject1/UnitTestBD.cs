﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using diplom_work;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTestBD
    {
        [TestMethod]
        public void TestMethodMakeRequest()
        {
            Query CheckConnect = new Query();
            SpacePoint sp = new SpacePoint(35,45,6);
            if (CheckConnect.QuerySpacePoint(sp) == null)
                Assert.Fail();
        }
        
        [TestMethod]
        public void TestMethodMakeNullRequest()
        {
            Query CheckConnect = new Query();
            SpacePoint sp = new SpacePoint(0,0,0);
            SpacePoint sp1;
            try
            {
                sp1 = CheckConnect.QuerySpacePoint(sp);
            }
            catch
            {
                return;
            }
            if (sp1!=null)
                Assert.Fail();
        }

        [TestMethod]
        public void TestMethodMakeNullListRequest()
        {
            Query CheckConnect = new Query();
            SpacePoint sp = new SpacePoint(35, 45, 6);
            if (CheckConnect.QuerySkyMap(sp, 0, 0) == null)
                Assert.Fail();
        }

        [TestMethod]
        public void TestMethodNullStarRequest()
        {
            Query CheckConnect = new Query();
            string StarName = "Cas";
            if (CheckConnect.QueryStar(StarName) != null)
                Assert.Fail();
        }
        [TestMethod]
        public void TestMethodStarRequest()
        {
            Query CheckConnect = new Query();
            string StarName = "32 Cyg";
            if (CheckConnect.QueryStar(StarName) == null)
                Assert.Fail();
        }
        
    }    
}
