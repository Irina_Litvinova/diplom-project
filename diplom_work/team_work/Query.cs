﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Net;
using System.Globalization;

namespace diplom_work
{
    public class Query
    {
        private const int MaxServerNumber = 8;
        
        private string getServerGroup (int num)
        {
            return "http://server" + num.ToString() + ".sky-map.org/getstars.jsp?";
        }

        private string getServerStar(int num)
        {
            return "http://server" + num.ToString() + ".sky-map.org/search?"; 
        }

        public static NumberFormatInfo NFI()
        {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            return nfi;
        }

        public List<SpacePoint> QuerySkyMap(SpacePoint sp, float angle, int maxStars)
        {
            for (int i = 0; i < MaxServerNumber; i++)
            {
                try
                {
                    string request = CreateRequest(sp, angle, maxStars, getServerGroup(i));
                    XmlDocument response = MakeRequest(request);
                    List<SpacePoint> list = ParseGroup(response);
                    return list;
                }
                catch (System.Net.WebException e)
                {

                }
            }
            return null;
        }

        public SpacePoint QuerySpacePoint(SpacePoint sp)
        {
            for (int i = 0; i < MaxServerNumber; i++)
            {
                try
                {
                    string request = CreateRequest(sp, 1,1, getServerGroup(i));
                    XmlDocument response = MakeRequest(request);
                    SpacePoint list = ParseSpacePoint(response);
                    if (list==null)
                    {
                        request = CreateRequest(sp, 10, 1, getServerGroup(i));
                        response = MakeRequest(request);
                        list = ParseSpacePoint(response);
                    }
                    return list;
                }
                catch (System.Net.WebException e)
                {

                }
            }
            return null;
        }

        public Star QueryStar(String starName)
        {
            for (int i = 0; i < MaxServerNumber; i++)
            {
                try
                {
                    string request = CreateRequest(starName, getServerStar(i));
                    XmlDocument response = MakeRequest(request);

                    Star star = ParseStar(response);
                    return star;
                }
                catch (System.Net.WebException e)
                {

                }
            }
            return null;
        }

        private static string CreateRequest(String starName, string Server)
        {
            string request = Server;
            request += "star=" + starName;
            return request;
        }
       

        public static string CreateRequest(SpacePoint sp, float angle, int maxStars, string Server)
        {
            string request = Server;
            request += "ra=" +sp.RA.ToString().Replace(',','.') + "&";
            request += "de=" + sp.DE.ToString().Replace(',', '.') + "&";
            request += "max_vmag=" + sp.Magnitude.ToString().Replace(',', '.') + "&";
            request += "angle=" + angle + "&";
            request += "max_stars=" + maxStars;
            return request;
        }

        private static string ToMD5(string request)
        {
            string result = request;
            result = result.Replace('&', '_');
            result = result.Replace('?', '_');
            result = result.Replace('.', '_');
            result = result.Replace('/', '_');
            result = result.Replace(':', '_');
            return result;

        }
        public static XmlDocument MakeRequest(string request)
        {
            string FileName = "";
            FileName = ToMD5(request);
            string Way = @"C:\Users\iren\Documents\Diplom_rep\diplom_work\cash\";
            Way += FileName + ".Xml";
            XmlDocument xmlDoc;
            if (File.Exists(Way))
            {
                XmlTextReader reader = new XmlTextReader(Way);
                xmlDoc = new XmlDocument();                
                xmlDoc.Load(reader);
                return xmlDoc;
            }
            HttpWebRequest httpRequest = WebRequest.Create(request) as HttpWebRequest;
            httpRequest.Timeout = 10000;
            HttpWebResponse httpResponse = httpRequest.GetResponse() as HttpWebResponse;

            xmlDoc = new XmlDocument();
            xmlDoc.Load(httpResponse.GetResponseStream());
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Save(Way);
            return xmlDoc;
        }

        public static Star ParseStar(XmlDocument xml)
        {
            try
            {
                XmlElement star = (XmlElement)xml.GetElementsByTagName("object")[0];
                //int id = int.Parse((star.Attributes["id"].Value));
                string catID = star.GetElementsByTagName("catId")[0].InnerText;

                XmlElement constellation = (XmlElement)star.GetElementsByTagName("constellation")[0];
                int constellationID = int.Parse(constellation.Attributes["id"].Value);
                string constellationName = constellation.InnerText;

                double ra = double.Parse(star.GetElementsByTagName("ra")[0].InnerText, NFI());
                double de = double.Parse(star.GetElementsByTagName("de")[0].InnerText, NFI());
                float mag = float.Parse(star.GetElementsByTagName("mag")[0].InnerText, NFI());
                return new Star(new SpacePoint(0, catID, ra, de, mag), new Constellation(constellationID, constellationName));
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static SpacePoint ParseSpacePoint(XmlDocument xml)
        {
            try
            {
                XmlElement star = (XmlElement)xml.GetElementsByTagName("response")[0];
                //int id = int.Parse((star.Attributes["id"].Value));
                string catID = star.GetElementsByTagName("catId")[0].InnerText;
                double ra = double.Parse(star.GetElementsByTagName("ra")[0].InnerText, NFI());
                double de = double.Parse(star.GetElementsByTagName("de")[0].InnerText, NFI());
                float mag = float.Parse(star.GetElementsByTagName("mag")[0].InnerText, NFI());
                SpacePoint point = new SpacePoint(0, catID, ra, de, mag);
                return point;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<SpacePoint> ParseGroup(XmlDocument xml)
        {
            List<SpacePoint> list = new List<SpacePoint>();
            XmlNodeList stars = xml.GetElementsByTagName("star");

            foreach (XmlElement star in stars)
            {
                int id = int.Parse((star.Attributes["id"].Value));
                string catID = star.GetElementsByTagName("catId")[0].InnerText;
                double ra = double.Parse(star.GetElementsByTagName("ra")[0].InnerText, NFI());
                double de = double.Parse(star.GetElementsByTagName("de")[0].InnerText, NFI());
                float mag = float.Parse(star.GetElementsByTagName("mag")[0].InnerText, NFI());
                list.Add(new SpacePoint(id, catID, ra, de, mag));
            }
            return list;
        }

        
    }
    
}
