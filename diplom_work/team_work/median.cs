﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace diplom_work
{
    class median
    {
        public Bitmap Img;
        private Bitmap Img2;
        public int rad;

        public median(ToolStripProgressBar Pb, Bitmap Image, int rad)
        {
            this.Img = (Bitmap)Image.Clone();
            this.Img2 = (Bitmap)Image.Clone();
            this.rad = rad;
            
            
        }
        private int partition(int[] a, int p, int r)
        {
            int x = a[r];
            int i = p - 1;
            int tmp;
            for (int j = p; j < r; j++)
            {

                if (a[j] <= x)
                {
                    i++;
                    tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;

                }
            }
            tmp = a[r];
            a[r] = a[i + 1];
            a[i + 1] = tmp;
            return (i + 1);

        }


        private void quicksort(int[] a, int p, int r)
        {
            if (p < r)
            {
                int q = partition(a, p, r);
                quicksort(a, p, q - 1);
                quicksort(a, q + 1, r);
            }
        }
        
        public void median_filter(ToolStripProgressBar Pb, int x, int y)
        {
            int n;
            int cR_, cB_, cG_;
            int k = 1;

            n = (2 * rad + 1) * (2 * rad + 1);


            int[] cR = new int[n + 1];
            int[] cB = new int[n + 1];
            int[] cG = new int[n + 1];

            for (int i = 0; i < n + 1; i++)
            {
                cR[i] = 0;
                cG[i] = 0;
                cB[i] = 0;
            }

            int w_b = Img2.Width;
            int h_b = Img2.Height;

            for (int i = x - rad; i < x + rad+1; i++)
            {
                for (int j = y - rad; j < y + rad+1; j++)
                {
                    System.Drawing.Color c = Img2.GetPixel(i, j);
                    cR[k] = System.Convert.ToInt32(c.R);
                    cG[k] = System.Convert.ToInt32(c.G);
                    cB[k] = System.Convert.ToInt32(c.B);
                    k++;

                }
            }


            quicksort(cR, 0, n - 1);
            quicksort(cG, 0, n - 1);
            quicksort(cB, 0, n - 1);

            int n_ = (int)(n / 2) + 1;

            cR_ = cR[n_];
            cG_ = cG[n_];
            cB_ = cB[n_];

            Img.SetPixel(x, y, System.Drawing.Color.FromArgb(cR_, cG_, cB_));
        }
    }
}
