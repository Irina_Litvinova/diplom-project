﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace diplom_work
{
    public class sharpen
    {
        private static Double[,] firstImage;
        public Double[,] secondImage;

        public sharpen(Double [,] Img)
        {
            firstImage = Img;
            secondImage = new double[Img.GetLength(0), Img.GetLength(1)];
            for (int i = 0; i < Img.GetLength(0); i++)
                for (int j = 0; j < Img.GetLength(1); j++)
                    secondImage[i, j] = Img[i, j];
        }

        public Double[,] Sobel(ToolStripProgressBar Pb)
        {
            Pb.Minimum = 1; Pb.Step = 1; Pb.Maximum = ((firstImage.GetLength(0) - 2) * (firstImage.GetLength(1) - 2)); Pb.Value = 1;
            for (int i = 1; i < firstImage.GetLength(0)-2; i++)
                for (int k = 1; k < firstImage.GetLength(1)-2; k++ )
                {
                    double Gx = (firstImage[i+1,k-1]+2*firstImage[i+1,k]+firstImage[i+1,k+1]) -
                        (firstImage[i-1,k-1]+2*firstImage[i-1,k]+firstImage[i-1,k+1]);
                    double Gy = (firstImage[i-1,k+1]+2*firstImage[i,k+1]+firstImage[i+1,k+1])-
                        (firstImage[i-1,k-1]+2*firstImage[i,k-1]+firstImage[i+1,k-1]);
                    double rab = Math.Min(Math.Sqrt(Math.Pow(Gx,2)+Math.Pow(Gy,2)),255);
                    secondImage[i,k]  += rab;
                    Pb.PerformStep();
                }
            return secondImage;
        }

        public Double[,] Privett(ToolStripProgressBar Pb)
        {
            Pb.Minimum = 1; Pb.Step = 1; Pb.Maximum = ((firstImage.GetLength(0) - 2) * (firstImage.GetLength(1) - 2)); Pb.Value = 1;
            for (int i = 1; i < firstImage.GetLength(0) - 2; i++)
                for (int k = 1; k < firstImage.GetLength(1) - 2; k++)
                {
                    double Gx = (firstImage[i + 1, k - 1] + firstImage[i + 1, k] + firstImage[i + 1, k + 1]) -
                        (firstImage[i - 1, k - 1] + firstImage[i - 1, k] + firstImage[i - 1, k + 1]);
                    double Gy = (firstImage[i - 1, k + 1] + firstImage[i, k + 1] + firstImage[i + 1, k + 1]) -
                        (firstImage[i - 1, k - 1] + firstImage[i, k - 1] + firstImage[i + 1, k - 1]);

                    secondImage[i, k] += Math.Min(Math.Sqrt(Math.Pow(Gx, 2) + Math.Pow(Gy, 2)),255);
                    Pb.PerformStep();
                }
            return secondImage;
        }

        public Double[,] Roberts(ToolStripProgressBar Pb)
        {
            Pb.Minimum = 1; Pb.Step = 1; Pb.Maximum = ((firstImage.GetLength(0) - 2) * (firstImage.GetLength(1) - 2)); Pb.Value = 1;
            for (int i = 1; i < firstImage.GetLength(0) - 2; i++)
                for (int k = 1; k < firstImage.GetLength(1) - 2; k++)
                {
                    double Gx = (firstImage[i , k] - firstImage[i + 1, k+1]);
                    double Gy = (firstImage[i, k + 1] - firstImage[i-1, k]);
                    secondImage[i, k] += Math.Min(Math.Abs(Gx) + Math.Abs(Gy),255);
                    Pb.PerformStep();
                }
            return secondImage;
        }



    }
}
