﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace diplom_work
{
    class Identity
    {
        private Star[] ImgStar;                   //список выделенных звезд на изображении
        private List<SpacePoint> CatStar;         //список звезд из каталога
        public Star[] RefImg;                     //список опорных звезд на изображении 
        private SpacePoint[] RefCat;              //список опорных звезд из каталога
        private SpacePoint Central;               //центральная звезда
        private double e;                         //допустимое расхождение
        private int[,] table;                     //таблица соответствий 
        public Star[,] Constelation;              //матрица созвездий
        int Method;                               //идентификатор метода
        double focal;                             //фокусное расстояние
        public double A0, A1, A2, B0, B1, B2;     //коэффициенты масштаба

        public Identity(Star[] stars, List<SpacePoint> list, SpacePoint sp, ToolStripProgressBar pb, double e, int Method, double Focal)
        {
            this.CatStar = list;
            this.ImgStar = stars;
            this.Central = sp;
            this.e = e;
            this.focal = Focal;
            this.Method = Method;
            
        }

        public void Calculate(ToolStripProgressBar pb)
        {
            pb.Value = 1;
            pb.Minimum = 1;
            pb.Maximum = 10;
            pb.Step =1;
            CheckRef(pb);                //выбор опорных звезд
            pb.PerformStep();
            switch (Method)
            {
                case 0:
                    TriangleCalculate();  //расчет таблицы соответствий
                    pb.PerformStep();
                    Conformity();
                    break;
                case 1:
                    StarAngle();
                    pb.PerformStep();
                    Conformity();
                    break;
                case 2:
                    CombiedMethod();
                    pb.PerformStep();
                    Conformity();
                    break;
            }
            pb.PerformStep();
            FindConstelation();
            pb.PerformStep();
            CalculateCoef();
            pb.PerformStep();
            CalculateCoefnu();
            pb.PerformStep();
        }

        private void CheckRef(ToolStripProgressBar pb)
        {
            pb.Minimum = 1;
            pb.Maximum = 10;
            //выбор опорных звезд из каталога
            CatStar.Sort(delegate(SpacePoint cs1, SpacePoint cs2)
            { return cs1.Magnitude.CompareTo(cs2.Magnitude); });
            int N = 15;
            if (CatStar.Count < N) N = CatStar.Count;
            if (ImgStar.GetLength(0) < N) N = ImgStar.GetLength(0);
            this.RefImg = new Star[N];
            this.RefCat = new SpacePoint[N];
            pb.PerformStep();
            for (int i=0; i<RefCat.GetLength(0); i++)
            {
                RefCat[i] = CatStar[RefCat.GetLength(0) - 1 - i];
                RefCat[i].Calculate(Central.RA, Central.DE);
            }
            pb.PerformStep();
            //выбор опорных звезд на изображении
            Sort();
            for (int i=0; i<RefImg.GetLength(0); i++)
            {
                RefImg[i] = ImgStar[i];
                double x = RefImg[i].X / 100; double y = RefImg[i].Y / 100;
                RefImg[i].X = x; RefImg[i].Y = y;     
            }
            pb.PerformStep();
            //создание таблицы соответствий
            table = new int[N, N];
            for (int i = 0; i < N; i++)
                for (int j = 0; j < N; j++)
                    table[i, j] = 0;
        }

        private void TriangleCalculate()
        {
            for (int iCat=0; iCat<RefCat.GetLength(0)-2;iCat++)
                for (int jCat=iCat+1; jCat<RefCat.GetLength(0)-1; jCat++)
                    for (int kCat=jCat+1; kCat<RefCat.GetLength(0);kCat++)
                    {
                        Triangle Cat = new Triangle(RefCat[iCat], RefCat[jCat], RefCat[kCat], Central);
                        for (int iImg=0; iImg<RefImg.GetLength(0)-2;iImg++)
                            for (int jImg=iImg+1; jImg<RefImg.GetLength(0)-1; jImg++)
                                for (int kImg = jImg+1; kImg < RefImg.GetLength(0); kImg++)
                                {
                                    Triangle Img = new Triangle(RefImg[iImg], RefImg[jImg], RefImg[kImg]);
                                    if ((Math.Abs(Cat.p - Img.p) < e) & (Math.Abs(Cat.q - Img.q) < e)) //соответствие найдено
                                    {
                                        table[iCat, iImg] += 1; 
                                        table[jCat, jImg] += 1; 
                                        table[kCat, kImg] += 1; 
                                    }
                                }

                    }
            
        }


        private void Conformity()
        {
            for (int i = 0; i < table.GetLength(0); i++)
            {
                double max = 0;
                int img = 0;
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    if (RefImg[j].SpcPoint==null)
                    if (table[i, j] >= max)
                    {
                            max = table[i, j];
                            img = j;
                    }
                }
                RefImg[img].SpcPoint = RefCat[i];
                RefImg[img].Constellate();
            }
        }

        private void StarAngle()
        {
            for (int i = 0; i < table.GetLength(0)-1; i++)
                for (int j = 0; j < table.GetLength(1); j++)
                    table[i, j] = 0;
            for (int i = 0; i < RefImg.GetLength(0) - 1; i++)
                for (int j = i + 1; j < RefImg.GetLength(0); j++)
                {
                    double RefCos = CalculateRef(RefImg[i], RefImg[j]);
                    for (int k = 0; k < RefCat.GetLength(0) - 1; k++)
                        for (int l = k + 1; l < RefCat.GetLength(0); l++)
                        {
                            double RefCos2 = CalculateCat(RefCat[k], RefCat[l]);
                            if (Math.Abs(RefCos - RefCos2) < e)
                            {
                                table[i, k] += 1;
                                table[j, l] += 1;
                                table[j, k] += 1;
                                table[i, l] += 1;
                            }
                        }
                }
        }

        private double CalculateRef(Star i, Star j)
        {
            double Cos = 0;
            double Divj = (Math.Sqrt(Math.Pow(j.X, 2) + Math.Pow(j.Y, 2) + Math.Pow(focal, 2)));
            double Divi = (Math.Sqrt(Math.Pow(i.X, 2) + Math.Pow(i.Y, 2) + Math.Pow(focal, 2)));
            double Li = i.X/Divi;             double Lj = j.X/Divj;
            double Mi = i.Y /Divi;            double Mj = j.Y /Divj;
            double Ni = focal / Divi;         double Nj = focal / Divj;
            Cos = Li * Lj + Mi * Mj + Ni * Nj;
            return Cos;
        }

        private double CalculateCat(SpacePoint i, SpacePoint j)
        {
            double Cos = 0;
            double Li = Math.Cos(i.DE) * Math.Cos(i.RA); double Lj = Math.Cos(j.DE) * Math.Cos(j.RA);
            double Mi = Math.Cos(i.DE) * Math.Sin(i.RA); double Mj = Math.Cos(j.DE) * Math.Sin(j.RA);
            double Ni = Math.Sin(i.DE);                  double Nj = Math.Sin(j.DE);
            Cos = Li * Lj + Mi * Mj + Ni * Nj;
            return Cos;
        }

        private void CombiedMethod()
        {
            if (focal!=0)
            {
                StarAngle();
            }
            else
            {
                TriangleCalculate();
                Conformity();
                focal = 1 / Math.Tan(CalculateCoef());
                StarAngle();
            }
        }

        

        private void Sort()
        {
            for (int i=0; i<ImgStar.Length; i++)
            {
                for (int j=0; j<ImgStar.Length-1; j++)
                {
                    if (ImgStar[j].A < ImgStar[j+1].A)
                    {
                        Star rab = ImgStar[j];
                        ImgStar[j] = ImgStar[j + 1];
                        ImgStar[j + 1] = rab; 
                    }
                }
            }
        }

        public void FindConstelation()
        {
            Star[] Rab = new Star[RefImg.GetLength(0)];
            RefImg.CopyTo(Rab,0);
            Constelation = new Star[RefImg.GetLength(0), RefImg.GetLength(0)];
            int k = 0;
            int d = 0;
            for (int i=0; i<Rab.GetLength(0); i++)
            {
                if (Rab[i].SpcPoint != null)
                {
                    d = 0;
                    if (Rab[i].Constellation != null)
                    {
                        Constelation[k, d] = Rab[i];
                        k++;
                        for (int j = i + 1; j < Rab.GetLength(0); j++)
                        {
                            if (Rab[j].Constellation != null)
                                if (Rab[i].Constellation.Name == Rab[j].Constellation.Name)
                                {
                                    d++;
                                    Constelation[k, d] = Rab[j];
                                    //Rab[j].SpcPoint = null;
                                }
                        }
                    }
                }

            }
        }

        private double CalculateCoef()
        {
            int n = RefImg.Length;
            double[] b1 = new double[n];
            for (int i = 0; i < n; i++)
                b1[i] = RefCat[i].ksi;
            double[,] an = new double[n , 3];
            for (int i = 1; i < n; i++)
            {
                an[i, 0] = 1;
                an[i, 1] = Math.Abs(RefImg[i].X); 
                an[i, 2] = Math.Abs(RefImg[i].Y);
            }
            LSM solve = new LSM(an, b1);
            double[] a = new double[solve.Ans.GetLength(0)];
            a = solve.Ans;
            A0 = a[0]; A1 = a[1]; A2 = a[2];
            double m = 0;
            m = Math.Sqrt(Math.Pow(a[1], 2) + Math.Pow(a[2], 2));
            return m;
            //CalculateCoord(a);        //рассчет координат опорных звезд
        }
        private void CalculateCoefnu()
        {
            int n = RefImg.Length;
            double[] b1 = new double[n];
            for (int i = 0; i < n; i++)
                b1[i] = RefCat[i].nu;
            double[,] an = new double[n, 3];
            for (int i = 1; i < n; i++)
            {
                an[i, 0] = 1;
                an[i, 1] = Math.Abs(RefImg[i].X);
                an[i, 2] = Math.Abs(RefImg[i].Y);
            }
            LSM solve = new LSM(an, b1);
            double[] a = new double[solve.Ans.GetLength(0)];
            a = solve.Ans;
            B0 = a[0]; B1 = a[1]; B2 = a[2];
            //CalculateCoord(a);        //рассчет координат опорных звезд
        }

        private void CalculateCoord(double[,] a)
        {
            for (int i=1; i<RefImg.Length; i++)
            {
                double ksi = 0;
                double nu = 0;
                ksi += Math.Abs(RefImg[0].X - RefImg[i].X)*a[i,0] + Math.Abs(RefImg[0].Y - RefImg[i].Y) * a[i, 1];
                nu += Math.Abs(RefImg[0].X - RefImg[i].X)*a[i,0] + Math.Abs(RefImg[0].Y - RefImg[i].Y) * a[i, 1];
                
                double RA = 0;
                double DE = 0;
                double d = ksi / (1 - nu * Math.Sin(Central.DE));
                RA = Central.RA + Math.Atan(d);
                double e = (Math.Cos(RA - Central.RA) * (nu + Math.Tan(Central.DE)))/(1-nu*Math.Tan(Central.DE));
                DE = Math.Atan(e);
                RefImg[i].SpcPoint = new Query().QuerySpacePoint(new SpacePoint(Math.Round(RA, 4), Math.Round(DE, 4), Convert.ToSingle(RefImg[i].A)));
                
            }
        }

    }
    public class IdealCoor
    {
        public double ksi, nu;
        SpacePoint Central;
        public IdealCoor(SpacePoint x, SpacePoint Centr)
        {
            Central = Centr;
            this.ksi = CalculateKsi(x);
            this.nu = CalculateNu(x);
        }
        private double CalculateKsi(SpacePoint x)
        {
            double ksi = 0;
            ksi = Math.Cos(x.DE) * Math.Sin(x.RA - Central.RA);
            double rab = Math.Sin(x.DE) * (Math.Sin(Central.DE) + Math.Cos(x.DE) * Math.Cos(Central.DE) * Math.Cos(x.RA - Central.RA) / Math.Sin(x.DE));
            ksi = ksi / rab;
            return ksi;
        }

        private double CalculateNu(SpacePoint x)
        {
            double nu = 0;
            nu = Math.Sin(x.DE) * Math.Cos(Central.DE) - Math.Cos(x.DE) * Math.Sin(Central.DE) * Math.Cos(x.RA - Central.RA);
            double rab = Math.Sin(x.DE) * (Math.Sin(Central.DE) + Math.Cos(x.DE) * Math.Cos(Central.DE) * Math.Cos(x.RA - Central.RA) / Math.Sin(x.DE));
            nu = nu / rab;
            return nu;
        }
    }
    public class Triangle
    {
        public double p, q;
        double a, b, c;

        public Triangle(SpacePoint x, SpacePoint y, SpacePoint z, SpacePoint Central)
        {
            IdealCoor X = new IdealCoor(x, Central);
            IdealCoor Y = new IdealCoor(y, Central);
            IdealCoor Z = new IdealCoor(z, Central);
            this.a = Distance(X.ksi, X.nu, Y.ksi, Y.nu);
            this.b = Distance(Y.ksi, Y.nu, Z.ksi, Z.nu);
            this.c = Distance(Z.ksi, Z.nu, X.ksi, X.nu);
            this.q = this.a/this.c;
            this.p = this.b / this.c;
        }

        public Triangle(Star X, Star Y, Star Z)
        {
            this.a = Distance(X.X, X.Y, Y.X, Y.Y);
            this.b = Distance(Y.X, Y.Y, Z.X, Z.Y);
            this.c = Distance(Z.X, Z.Y, X.X, X.Y);
            this.q = this.a / this.c;
            this.p = this.b / this.c;
        }
        public double Distance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow((x1 - x2), 2) + Math.Pow((y1 - y2), 2));
        }

    }
}
