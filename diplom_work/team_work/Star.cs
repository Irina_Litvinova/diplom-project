﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace diplom_work
{
    public class Star
    {
        public double X, Y;
        public Double[] I;
        public double R;
        public int Id;
        public Point[] Points;
        public double A;
        public SpacePoint SpcPoint;
        public Constellation Constellation;
        int Profil;

        public Star(double r,Double[] i, int x, int y,Point[] point, int profil)
        {
            this.R = r;
            I = new Double[i.Length]; I = i;
            this.X = x;
            this.Y = y;
            this.Points = point;
            this.A = 0;
            Profil = profil;
            Calculate();
        }

        private double max()
        {
            double res = 0;
            for (int i = 0; i < I.Length; i++ )
            {
                if (I[i] > res)
                    res = I[i];
            }
                return res;
        }

        public void Calculate()
        {
            int K = I.Length;
            double C = max();
            for (int j=0; j<K;j++)
            {
                double Rj;
                Rj= Math.Pow((Points[j].X - X),2) + Math.Pow((Points[j].Y - Y),2);
                double An;
                if (Profil == 1)
                    An = Math.Pow((C / I[j]), 0.714) - 1;
                else
                    An = C / I[j] - 1;
               if (Rj!=0)  
                   if (Profil==0)
                       An = An / (Math.Pow(Rj,1.4));
                   else
                       An = An / Rj;
               A = A + An ;      
               //A += I[j];
            }
            A = A / K;
            A = C / (1 + A * Math.Pow(10, 1.4));
            
        }
        public static int compareByName(SpacePoint x, SpacePoint y)
        {
            if ((x == null) || (y == null))
                return 0;
            return x.CatID.CompareTo(y.CatID);
        }

        public static int compareByMag(SpacePoint x, SpacePoint y)
        {
            if ((x == null) || (y == null))
                return 0;
            return x.Magnitude.CompareTo(y.Magnitude);
        }

        public static int compareByA(Star x, Star y)
        {
            if ((x == null) || (y == null))
                return 0;
            return x.A.CompareTo(y.A);
        }

        public static int compareById(Star x, Star y)
        {
            if ((x == null) || (y == null))
                return 0;
            return x.Id.CompareTo(y.Id);
        }

        public Star(SpacePoint sp, Constellation constellation)
        {
            this.SpcPoint = sp;
            this.Constellation = constellation;
        }

        public void Constellate()
        {
            if (SpcPoint!=null)
            {
                Star star = new Query().QueryStar(SpcPoint.CatID);
                if (star!=null) 
                    this.Constellation = star.Constellation;
            }
        }
        
    }
    public class Constellation
    {
        public Constellation(int id, string name)
        {
            this.ID = id;
            this.Name = name;
        }

        public readonly int ID;
        public readonly string Name;
    }
}
