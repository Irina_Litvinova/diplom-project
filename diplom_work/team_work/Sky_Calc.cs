﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace diplom_work
{
    
    public class Sky_Calc
    {
        public  Bitmap Image;
        public Star[] stars = new Star[0];
        private Double[,] Img_Int;
        Point min = new Point();
        Point max = new Point();
        Point[] points;
        Double[] star;
        int Sensetive = 90;
        int Sharp, Profil;
        ToolStripProgressBar Pb;
        public Sky_Calc(ToolStripProgressBar pb, Bitmap Img, int sharp, int profil)
        {
            Image = Img;
            Img_Int = new Double[Image.Width, Image.Height];
            Sharp = sharp; Profil = profil;
            Pb = pb;
        }

        public void Intensive(Bitmap Img)
        {
            Double[,] intens = new Double[Img.Width, Img.Height];
            int Height = Img.Height;
            object[] lockBuffer = new object[Height+1];
            for (var i = 0; i < lockBuffer.Length; i++)
                 {
                      lockBuffer[i] = new object();
                 }
             for (int i = 0; i < Image.Width; i++)
             {
                 Parallel.For(0, Image.Height, j =>
                 {
                     lock (lockBuffer[Height])
                     {
                         Color pixelColor = Image.GetPixel(i, j);
                         intens[i, j] = 0.3 * pixelColor.R + 0.5 * pixelColor.G + 0.1 * pixelColor.B;

                     }

                 });
                 Pb.PerformStep();
                 
             }
             Img_Int =  intens;
        }

        private void Check(int X, int Y)
        {
            if ((X<Img_Int.GetLength(0))&(Y<Img_Int.GetLength(1))&(X>0)&(Y>0))
            if (Img_Int[X, Y] > Sensetive)
            {
                if (X<min.X)
                    min.X = X; 
                if (Y<min.Y)
                    min.Y = Y;
                if ((X > max.X))
                     max.X = X;
                if (Y>max.Y)
                    max.Y = Y;
                
                Array.Resize(ref star, star.Length + 1);
                star[star.Length-1] = Img_Int[X, Y];
                Array.Resize(ref points, points.Length + 1);
                points[points.Length - 1].X = X;
                points[points.Length - 1].Y = Y;
                Img_Int[X, Y] = 0;
                Check(X, Y + 1);  Check(X, Y - 1);
                Check(X + 1, Y);  Check(X - 1, Y);
            }
        }



        public void Divide()
        {
            for (int i = 1; i<Img_Int.GetLength(0)-1; i++)
                for (int j = 1; j < Img_Int.GetLength(1)-1;j++)
                {
                    if (Img_Int[i, j] >Sensetive)
                    {                       
                        min.X = i; min.Y = j; max = min;
                        star = new Double[0];
                        points = new Point[0];
                        Check(i, j);
                        if (star.Length > 3)
                        {
                            Array.Resize(ref stars, stars.Length + 1);
                            double r = (max.X - min.X) / 2;
                            stars[stars.Length - 1] = new Star(r, star, min.X + Convert.ToInt32(r), min.Y + Convert.ToInt32(r), points, Profil);
                            stars[stars.Length - 1].Id = stars.Length - 1;
                        }
                        Pb.PerformStep();
                    }
                }
        }

        public void Sharped()
        {
            sharpen Sharped = new sharpen(Img_Int);
            switch(Sharp)
            {
                case 0:
                    Sharped.Sobel(Pb);
                    break;
                case 1:
                    Sharped.Roberts(Pb);
                    break;
                case 2:
                    Sharped.Privett(Pb);
                    break;
                case 3:
                    Sharped.Privett(Pb);
                    break;
                case 4:
                    break;
            }
            Img_Int = Sharped.secondImage;

        }
        public void Calculate(ToolStripStatusLabel Status)
        {            
            Intensive(Image);
            Status.Text = "Улучшение резкости";
            Sharped();
            Status.Text = "Выделение звезд на изображении";
            Pb.Value = 1; Pb.Step = 1; Pb.Maximum = Img_Int.GetLength(0) * Img_Int.GetLength(1);
            Divide();
          
        }
    }
}
