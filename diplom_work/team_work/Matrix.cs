﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace в
{
    class Matrix
    {
        public double[,] Args { get; set; }
        public int Row { get; set; }
        public int Col { get; set; }

        public Matrix(double[] x)
        {
            Row = x.Length;
            Col = 1;
            Args = new double[Row, Col];
            for (int i = 0; i < Args.GetLength(0); i++)
                for (int j = 0; j < Args.GetLength(1); j++)
                    Args[i, j] = x[i];
        }

        public Matrix(double[,] x)
        {
            Row = x.GetLength(0);
            Col = x.GetLength(1);
            Args = new double[Row, Col];
            for (int i = 0; i < Args.GetLength(0); i++)
                for (int j = 0; j < Args.GetLength(1); j++)
                    Args[i, j] = x[i, j];
        }

        public Matrix(Matrix other)
        {
            this.Row = other.Row;
            this.Col = other.Col;
            Args = new double[Row, Col];
            for (int i = 0; i < Row; i++)
                for (int j = 0; j < Col; j++)
                    this.Args[i, j] = other.Args[i, j];
        }

        public override string ToString()
        {
            string s = string.Empty;
            for (int i = 0; i < Args.GetLength(0); i++)
            {
                for (int j = 0; j < Args.GetLength(1); j++)
                {
                    s += string.Format("{0} ", Args[i, j]);
                }
                s += "\n";
            }
            return s;
        }

        public Matrix Transposition()
        {
            double[,] t = new double[Col, Row];
            for (int i = 0; i < Row; i++)
                for (int j = 0; j < Col; j++)
                    t[j, i] = Args[i, j];
            return new Matrix(t);
        }

        public static Matrix operator *(Matrix m, double k)
        {
            Matrix ans = new Matrix(m);
            for (int i = 0; i < ans.Row; i++)
                for (int j = 0; j < ans.Col; j++)
                    ans.Args[i, j] = m.Args[i, j] * k;
            return ans;
        }

        public static double[] operator *(Matrix m, double[] b)
        {
            double[] ans = new double[b.Length];
            for (int i = 0; i < m.Row; i++)
                for (int j = 0; j < m.Col; j++ )
                {
                    ans[i] = ans[i] + m.Args[i, j] * b[i];
                }
             return ans;
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            //if (m1.Col != m2.Row) throw new ArgumentException("Multiplication of these two matrices can't be done!");
            double[,] ans = new double[m2.Row, m1.Col];
            for (int i = 0; i < m2.Row; i++)
            {
                for (int j = 0; j < m1.Col; j++)
                {
                    for (int k = 0; k < m2.Row; k++)
                    {
                        ans[i, j] += m1.Args[i, k] * m2.Args[k, j];
                    }
                }
            }
            return new Matrix(ans);
        }

        private Matrix getMinor(int row, int column)
        {
            if (Row != Col) throw new ArgumentException("Matrix should be square!");
            double[,] minor = new double[Row - 1, Col - 1];
            for (int i = 0; i < this.Row; i++)
            {
                for (int j = 0; j < this.Col; j++)
                {
                    if ((i != row) || (j != column))
                    {
                        if (i > row && j < column) minor[i - 1, j] = this.Args[i, j];
                        if (i < row && j > column) minor[i, j - 1] = this.Args[i, j];
                        if (i > row && j > column) minor[i - 1, j - 1] = this.Args[i, j];
                        if (i < row && j < column) minor[i, j] = this.Args[i, j];
                    }
                }
            }
            return new Matrix(minor);
        }

        private static Matrix DetM(Matrix m)
        {
            Matrix a = new Matrix(m);
            for (int i=0;i<m.Row;i++)
            {
                double v = a.Args[i, i];
                if (v != 0)
                for (int j=i;j<m.Col;j++)
                   a.Args[i, j] /= v;
                if (i!=(m.Row-1))
                for (int j = i+1; j < m.Row; j++)
                    for (int k = 0; k < m.Col; k++)
                        a.Args[j, k] -= a.Args[i, k] * a.Args[j, k];
            }
            return a;
        }

        public static Matrix RefM(Matrix m)
        {
            Matrix a = new Matrix(m);
            double[,] bet = new double[m.Row, m.Col];
            for (int i =0; i<m.Col; i++)
            {
                for (int j=0; j<m.Col; j++)
                {
                    if (i == j)
                        bet[i, j] = 1;
                    else
                        bet[i, j] = 0;
                }
            }
            Matrix b = new Matrix(bet);
            for (int i = 0; i < m.Row; i++)
            {
                double v = a.Args[i, i];
                if (v != 0)
                    for (int j = i; j < m.Col; j++)
                    {
                        a.Args[i, j] /= v;
                        b.Args[i, j] /= v;
                    }
                if (i != (m.Row - 1))
                    for (int j = i + 1; j < m.Row; j++)
                        for (int k = 0; k < m.Col; k++)
                        {
                            b.Args[j, k] -= b.Args[i, k] * a.Args[j, k];
                            a.Args[j, k] -= a.Args[i, k] * a.Args[j, k];
                            
                        }
            }
            for (int i = 0; i <a.Row-1;i++ )
            {
                for (int j=i; j<a.Col; j++)
                {
                    b.Args[a.Row - i - 2, a.Col - j-1] -= b.Args[a.Row - i - 1, a.Col - j-1] * a.Args[a.Row - i - 2, a.Col - j-1];
                    a.Args[a.Row - i - 2, a.Col - j-1] -= a.Args[a.Row - i - 1, a.Col - j-1] * a.Args[a.Row - i - 2, a.Col - j-1];
                }
            }
            Matrix d = m * b;
                return b;
        }
        public static double Determ(Matrix m)
        {
            if (m.Row != m.Col) throw new ArgumentException("Matrix should be square!");
            double det = 0;
            int length = m.Row;

            if (length == 1) det = m.Args[0, 0];
            if (length == 2) det = m.Args[0, 0] * m.Args[1, 1] - m.Args[0, 1] * m.Args[1, 0];
            Matrix x = new Matrix(DetM(m));
            if (length > 2)
                for (int i = 0; i < m.Col; i++)
                    det += Math.Pow(-1, 0 + i) * x.Args[i,i];

            return det;
        }

        public Matrix MinorMatrix()
        {
            double[,] ans = new double[Row, Col];

            for (int i = 0; i < Row; i++)
                for (int j = 0; j < Col; j++)
                    ans[i, j] = Math.Pow(-1, i + j) * Determ(this.getMinor(i, j));

            return new Matrix(ans);
        }

        public Matrix Inverse()
        {
            if (Math.Abs(Determ(this)) <= 0.000000001)
                throw new ArgumentException("Inverse matrix does not exist!");
            Matrix ans = RefM(this);

            return ans;
        }

        public Matrix InverseMatrix()
        {
            if (Math.Abs(Determ(this)) <= 0.000000001) 
                throw new ArgumentException("Inverse matrix does not exist!");

            double k = 1 / Determ(this);

            Matrix minorMatrix = this.MinorMatrix();
            minorMatrix = minorMatrix.Transposition();
            return minorMatrix * k;
        }
    }
}
