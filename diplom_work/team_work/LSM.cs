﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace diplom_work
{
    class LSM
    {
        public double[] Y { get; set; }


        public double[] Ans;
        public double[,] basic;

        public LSM(double[,] x, double[] ksi)
        {
            basic = x;
            Ans = new double[3];
            Y = ksi;
            Vector<double> AnsI = this.Polynomial(ksi.GetLength(0), Y);
            for (int j = 0; j < AnsI.Count; j++)
                Ans[j] = AnsI[j];
            
        }

        // Собственно, Метод Наименьших Квадратов
        // В качестве базисных функций используются степенные функции y = a0 * x^0 + a1 * x^1 + ... + am * x^m
        public Vector<double> Polynomial(int m, double[] y)
        {
            if (m <= 0) throw new ArgumentException("Порядок полинома должен быть больше 0");
            //if (m >= X.Length) throw new ArgumentException("Порядок полинома должен быть на много меньше количества точек!");
            
            // Создание матрицы из массива значений базисных функций(МЗБФ)
            Matrix<double> basicFuncMatr = DenseMatrix.OfArray(basic);
            
            // Транспонирование МЗБФ
            Matrix<double> transBasicFuncMatr = basicFuncMatr.Transpose();
            // Произведение транспонированного  МЗБФ на МЗБФ
            Matrix<double> lambda = transBasicFuncMatr * basicFuncMatr;

            // Произведение транспонированого МЗБФ на следящую матрицу 
            Matrix<double> beta = null;
            if (lambda.Determinant() < 0.000000000001)
            {
                //MessageBox.Show("Have not a inverse matrix!");
                double[] Z= new double[m]; 
                for (int i=0; i<m;i++)
                    Z[i] = 0; 
                Vector<double> err = DenseVector.OfArray(Z);
                return err;
            }
            else
                beta = lambda.Inverse();

            // Решение СЛАУ путем умножения обратной матрицы лямбда на бету
            Matrix<double> a = beta*transBasicFuncMatr;
            //умножаем результат на вектор b
            Vector<double> b= DenseVector.OfArray(y);
            Vector<double> answer = a*b;
            return answer;            
        }

       
    }
}
