﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System.Diagnostics;
using PFP.Imaging;

namespace diplom_work
{
    public partial class Main : Form
    {
        DateTime data = DateTime.Now;   //дата наблюдений
        double lon=0;     //долгота
        double lat=0;     //широта
        double teta = 0;
        int count = 100; //кол-во выводимых звезд
        List<SpacePoint> list;
        private Bitmap Img, firstImg;
        SpacePoint sp = new SpacePoint(20.41, 45.16, 8);
        private Star[] ImageStar;    //список выделенных звезд на изображении
        float angle = 25;
        int Sharp = 2, Method = 0, Profil = 1;
        double focal = 0, azimut = 30;
        double A0, A1, A2, B0, B1, B2;
        public Main()
        {
            InitializeComponent();
        }

        private void Main_MouseMove(object sender, MouseEventArgs e)
        {
            int X = e.Location.X; int Y = e.Location.Y;
            CoorX.Text = X.ToString();
            CoorY.Text = Y.ToString();
            string check = CheckStar(X, Y);
            if (check !="")
                toolTip1.Show(check, pictureBox1, X + 15, Y + 15,120);
            CoorX.Refresh(); CoorY.Refresh();
        }

        

        private string CheckStar(int X, int Y)
        {
            Point point = new Point(X, Y);
            string result =" ";
            if (ImageStar != null)
            {
                for (int i=0; i<ImageStar.Length;i++)
                {
                    if(ImageStar[i].Points.Contains(point))
                    {
                        if (ImageStar[i].SpcPoint!=null)
                            result =  ((ImageStar[i].SpcPoint.CatID).ToString());                          
                        return result;
                    }
                }
            }
            return "";
        }


        private void pictureBox1_Click(object sender, MouseEventArgs e)
        {
            int X = e.Location.X; int Y = e.Location.Y;
            CoorX.Text = X.ToString();
            CoorY.Text = Y.ToString();
            string check = CheckStar(X, Y);
            if (check != "")
            {
                Star star = new Query().QueryStar(check);
                label1.Text = "Информация о звезде: \n Имя звезды : " + check;  
                if (star!=null)
                if (star.SpcPoint!=null)
                    label1.Text += " \n Звездная величина: " + star.SpcPoint.Magnitude + "\n RA = " + star.SpcPoint.RA + "\n DE = " + star.SpcPoint.DE;
                if (star != null)
                {
                    label1.Text += "\n Имя созвездия: " + star.Constellation.Name;
                }
                else
                    MessageBox.Show("Дополнительная информация не найдена");
            }
            else
            {
                double ksi = A0+ A1 * e.X/100 + A2 * e.Y/100;
                double nu = B0+B1 * e.X/100 + A2 * e.Y/100;
                double RA = 0;
                double DE = 0;
                double d = ksi / (Math.Cos(sp.DE) * 57.2957795 - nu * Math.Sin(sp.DE) * 57.2957795);
                RA = sp.RA + Math.Atan(d) * 57.2957795;
                double en = (Math.Cos(RA - sp.RA) * 57.2957795 * (nu + Math.Tan(sp.DE) * 57.2957795)) / (1 - nu * Math.Tan(sp.DE) * 57.2957795);
                DE = Math.Atan(en) * 57.2957795;
                SpacePoint star = new Query().QuerySpacePoint(new SpacePoint(Math.Round(RA, 4), Math.Round(DE, 4),6));
                if (star != null)
                {
                    label1.Text = "Информация о звезде: \n Имя звезды : " + star.CatID;
                    label1.Text += " \n Звездная величина: " + star.Magnitude + "\n RA = " + star.RA + "\n DE = " + star.DE;
                }
                if (star != null)
                {
                    //label1.Text += "\n Имя созвездия: " + star.Constellation.Name;
                }
                else
                    MessageBox.Show("Дополнительная информация не найдена");
            }
            label1.Refresh();
        }
        private void Main_Load(object sender, EventArgs e)
        {
            data = new DateTime(2008, 7, 2,22,3,52);
            lon = 30.193;
            lat = 59.44;
            double date = data.ToUniversalTime().Hour + 0.01 * data.ToUniversalTime().Minute;
            teta = lon + date+date*0.0027379093;
            сохранитьToolStripMenuItem.Enabled = false;
            идентификацияToolStripMenuItem.Enabled = false;
            очиститьToolStripMenuItem.Enabled = false;
            видToolStripMenuItem.Enabled = false;
            данныеToolStripMenuItem.Enabled = false;
        }

        private void датаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Date date = new Date();
            date.date = new DateTime();
            date.date = data;
            date.ShowDialog();
            if (date.DialogResult == DialogResult.OK)
            {                
                data = date.date;
            }
        }

        private void местоположениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Place place = new Place();
            place.lat = lat;
            place.lon = lon;
            place.ShowDialog();
            if (place.DialogResult == DialogResult.OK)
            {
                lat = place.lat;
                lon = place.lon;
            }
        }


        private void выходToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
        }
        //
        //показать...
        //
        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string message =
        "Вы действительно хотите вывести на экран звездные объекты в данное время:"+
        data.ToString()+"\nВ месте с такими координатами Ш:"+lat.ToString()+" Д:"+
        lon.ToString()+"? \nКоличество объектов: "+count.ToString()
        +"\nИзменить выходные параметры можно на вкладке Данные главного меню.";
            const string caption = "Вычисления";
            var result = MessageBox.Show(message, caption,
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Pb.Visible = true;
                Pb.Minimum = 1; Pb.Maximum = 3; Pb.Value = 1;
                Status.Text = "Загрузка данных с сервера";
                double date = data.ToUniversalTime().Hour + 0.01 * data.ToUniversalTime().Minute;
                teta = lon + date + date * 0.0027379093;
                //sp = new SpacePoint(335, 59, angle);
                Pb.PerformStep();
                list = new Query().QuerySkyMap(sp, angle, count);
                Pb.PerformStep();
                if (list != null)
                    ShowList();
                else
                    MessageBox.Show("все серверы недоступны");
            }
            Status.Text = "Завершено";
            
        }

        private void ShowList()
        {
            listBox1.Items.Clear();
            // Shutdown the painting of the ListBox as items are added.
            listBox1.BeginUpdate();
            listBox1.Items.Add("CatID \t ID \t Magnitude");
            for (int i = 0; i < list.Count; i++)
            {
                listBox1.Items.Add(list[i].CatID.ToString() + " \t" + list[i].ID.ToString() +
                    " \t" + list[i].Magnitude.ToString() + " \t" + list[i].RA.ToString() +
                    " \t" + list[i].DE.ToString());
            }
            // Allow the ListBox to repaint and display the new items.
            listBox1.EndUpdate();
        }

        private void очиститьToolStripMenuItem_Click(object sender, EventArgs e)
        {
             var result = MessageBox.Show("Вы действительно хотите очистить все результаты?", "Очистить",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);

             if (result == DialogResult.Yes)
             {
                 if (list != null)
                     list.Clear();
                 listBox1.Items.Clear();
                 listBox2.Items.Clear();
                 Img = new Bitmap(firstImg); ;
                 pictureBox1.Image = Img;
                 Status.Text = "Завершено";
             }
        }

        private void количествоЗвездToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Count starCount = new Count();
            starCount.count = count;
            starCount.ShowDialog();
            if (starCount.DialogResult == DialogResult.OK)
            {
                count = starCount.count;
            }

        }

        private void руководствоПользователяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string message = "Для просмотра звездных объектов, необходимо ввести следующие данные:"+
                "\nКоординаты и время наблюдений (вкладка Данные главного меню), количество звездных "+
                "объектов, выводимых на экран (вкладка Вид главного меню). Можно выбрать фильтры,"+
                "накладываемые на выборку объектов (вкладка Вид главного меню). Для сортировки"+
                " выводы откройте в главном меню Вид - Сортировка по... и выберите признак для сортировки."+
                "Сама операция выводов списка звездных объектов находится на вкладке Файл - показать.."+
                "Если сразу после запуска выбрать данное действие, все данные будут выбраны по умолчанию.";
            MessageBox.Show(message,"Руководство пользователя");
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Info = listBox1.SelectedItem.ToString();
            string[] words = Info.Split('\t');
            Star star = new Query().QueryStar(words[0]);
            label1.Text = "Информация о звезде: \n Имя звезды : " + words[0] +
                " \n Звездная величина: " + words[2] + "\n RA = " + words[3] + "\n DE = " + words[4];
            if (star != null)
            {
                label1.Text += "\n Имя созвездия: " + star.Constellation.Name;
            }
            else
                MessageBox.Show("Дополнительная информация не найдена");
        }

        private void звезднойВеличинеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Status.Text = "Сортировка по звездной величине";
            Pb.Maximum = 2; Pb.Value = 1;
            list.Sort(Star.compareByMag);
            Pb.Value = 2;
            ShowList();
            звезднойВеличинеToolStripMenuItem.Checked = true;
            поАлфавитуToolStripMenuItem.Checked = false;
            Status.Text = "Завершено";
        }


        private void поАлфавитуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Status.Text = "Сортировка по алфавиту";
            Pb.Maximum = 2;  Pb.Value = 1;
            list.Sort(Star.compareByName);
            Pb.Value = 2;
            ShowList();
            поАлфавитуToolStripMenuItem.Checked = true;
            звезднойВеличинеToolStripMenuItem.Checked = false;
            Status.Text = "Завершено";
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "C:\\Users\\iren\\Pictures";
            openFileDialog1.Filter = "png files (*.png)|*.png|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string FileName = openFileDialog1.FileName;
                    pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
                    Img = new Bitmap(FileName);
                    Image img = Image.FromFile(FileName);
                    Meta_Data(img);
                    firstImg = new Bitmap(FileName);
                    pictureBox1.Image = Img;
                    pictureBox1.Invalidate();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
            
            сохранитьToolStripMenuItem.Enabled = true;
            идентификацияToolStripMenuItem.Enabled = true;
            очиститьToolStripMenuItem.Enabled = true;
            видToolStripMenuItem.Enabled = true;
            данныеToolStripMenuItem.Enabled = true;
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.IO.FileStream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = "C:\\Users\\iren\\Pictures";
            saveFileDialog1.Filter = "png files (*.png)|*.png|.*JPeg Image|*.jpg|Bitmap Image|*.bmp";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream =(System.IO.FileStream)saveFileDialog1.OpenFile()) != null)
                    {
                        switch (saveFileDialog1.FilterIndex)
                        {

                            case 1:
                                Img.Save(myStream,
                                   System.Drawing.Imaging.ImageFormat.Png);
                                break;
                            case 2:
                                Img.Save(myStream,
                                   System.Drawing.Imaging.ImageFormat.Jpeg);
                                break;

                            case 3:
                                Img.Save(myStream,
                                   System.Drawing.Imaging.ImageFormat.Bmp);
                                break;

                        };
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not write file from disk. Original error: " + ex.Message);
                }
            }
        }

        private bool CatStar()
        {
            Pb.Minimum = 1; Pb.Maximum = 3; Pb.Value = 1;
            Status.Text = "Загрузка данных с сервера"; statusStrip1.Refresh();
            double date = data.ToUniversalTime().Hour + 0.01 * data.ToUniversalTime().Minute;
            teta = lon + date + date * 0.0027379093;
            Pb.PerformStep();
            list = new Query().QuerySkyMap(sp, angle, count);
            Pb.PerformStep();
            Status.Text = "Завершено";
            if (list != null)
                ShowList();

            else
            {
                MessageBox.Show("все серверы недоступны");
                return true; 
            }
            return false;
        }

        private Star[] FindStar()
        {
            Sky_Calc Sky = new Sky_Calc(Pb, Img, Sharp, Profil);
            Pb.Maximum = Img.Height * Img.Width;
            Pb.Minimum = 1; Pb.Step = Img.Height; Pb.Value = 1;
            Status.Text = "Обработка изображения"; statusStrip1.Refresh();
            Sky.Intensive(Sky.Image);
            Status.Text = "Улучшение резкости"; statusStrip1.Refresh();           
            Sky.Sharped();                                    
            Status.Text = "Выделение звезд на изображении"; statusStrip1.Refresh();
            Pb.Value = 1; Pb.Step = 1; 
            Sky.Divide();
            //Sky.Calculate(Pb, Status);
            Img = Sky.Image;
            Star[] stars = Sky.stars;
            using (Graphics g = Graphics.FromImage(Img))
            {
                for (int i = 0; i < stars.Length; i++)
                {
                    int r = Convert.ToInt32(stars[i].R);
                    g.DrawRectangle(new Pen(Color.Red), Convert.ToInt32(stars[i].X - r), Convert.ToInt32(stars[i].Y - r), 2 * r + 5, 2 * r + 5);
                    //g.FillRectangle(new SolidBrush(Color.Green), Convert.ToInt32(stars[i].X), Convert.ToInt32(stars[i].Y), 1,1);
                }
                pictureBox1.Invalidate();
            }
            panel1.Invalidate();
            Status.Text = "Завершено"; statusStrip1.Refresh();
            return stars;
        }
        private void идентификацияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pb.Visible = true;
            if (CatStar()) 
                return;
            
            Status.Text = "Идентификация звездных объектов"; statusStrip1.Refresh();
            Pb.Value = 1; Pb.Minimum = 1; Pb.Maximum = 20;
            
            Identity identification = new Identity(FindStar(), list, sp, Pb,0.01,Method,focal);
            Status.Text = "Идентификация звездных объектов"; statusStrip1.Refresh();
            identification.Calculate(Pb);
            using (Graphics g = Graphics.FromImage(Img))
            {
                Point one, two;
                for (int i = 0; i < identification.RefImg.GetLength(0); i++)
                {
                    Point point = new Point();
                    point.X = Convert.ToInt32(identification.RefImg[i].X*100);
                    point.Y = Convert.ToInt32(identification.RefImg[i].Y*100);
                    //MessageBox.Show("id: " + i.ToString() + " Name " + identification.RefImg[i].SpcPoint.CatID);
                    //g.DrawString(identification.RefImg[i].SpcPoint.CatID+"  ", this.Font, Brushes.DarkRed, point);
                    listBox2.Items.Add(identification.RefImg[i].Id+"\t"+identification.RefImg[i].SpcPoint.CatID+"\t"
                        +point.X+"\t"+point.Y);
                    if (identification.Constelation[i,0] != null)
                    {
                        one = new Point(Convert.ToInt32(identification.Constelation[i,0].X*100),Convert.ToInt32(identification.Constelation[i,0].Y*10000));
                        for (int j = 0; j < identification.Constelation.GetLength(1); j++)
                        {
                            if (identification.Constelation[i,j]!=null)
                            {
                                two = new Point(Convert.ToInt32(identification.Constelation[i,j].X*100), Convert.ToInt32(identification.Constelation[i,j].Y*10000));
                                one = two;
                            }
                        }
                    }
                }

                pictureBox1.Invalidate();
            }
            Status.Text = "Завершено"; statusStrip1.Refresh();
            ImageStar = identification.RefImg;
            A0 = identification.A0; A1 = identification.A1; A2 = identification.A2;
            B0 = identification.B0; B1 = identification.B1; B2 = identification.B2;
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string message = "Информация о программе:" +
                "\nДанное приожение разработано в рамках дипломной работы " +
                "\n'Идентификация объектов звездного неба на изображении'" +
                "\nстудент: Литвинова Ирина Олеговна ИУ7-81 МГТУ им. Н. Э. Баумана" +
                "\nнаучный руководитель: Орлова Анастасия Андреевна ассистент кафедры ИУ7" +
                "\nконтакты: e-mail Литвиновой И. О.  iirraallii@gmail.com" +
                "\n2015 ";
            MessageBox.Show(message, "О программе");
        }
                        
        private void оригиналToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Anchor = (AnchorStyles.Top|AnchorStyles.Left);
            pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
            pictureBox1.Invalidate();
        }

        private void поРазмеруОкнаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.Invalidate();
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            CoorX.Text = "...";
            CoorY.Text = "...";
            toolTip1.Hide(pictureBox1);
            CoorX.Refresh(); CoorY.Refresh();
        }

        private void Main_ResizeBegin(object sender, EventArgs e)
        {
        }

        private void параметрыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filters = new Filters(focal);
            filters.ShowDialog();
            if (filters.DialogResult == DialogResult.OK)
            {
                //sp = new SpacePoint(filters.RA, filters.DE, 6);
                focal = filters.focal; azimut = filters.azimut;
                Sharp = filters.Sharp; Method = filters.Method;
                Profil = filters.Profil;
            }
        }

        private void выравниваниеФонаToolStripMenuItem_Click(object sender, EventArgs e)
        {

            
            Status.Text = "Выравнивание фона"; statusStrip1.Refresh();
            Pb.Minimum = 1;  Pb.Step = 1;
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            median Filter = new median(Pb, Img, 1);
            Pb.Maximum = (Img.Height - Filter.rad-1) * (Img.Width - Filter.rad-1);
            for (int x = Filter.rad + 1; x < Img.Width - Filter.rad; x++)
            {
                for (int y = Filter.rad + 1; y < Img.Height - Filter.rad; y++)
                {
                    Filter.median_filter(Pb, x, y);
                    Pb.PerformStep();
                }

            }
            Img = new Bitmap(Filter.Img);
            stopWatch.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopWatch.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            MessageBox.Show(elapsedTime); 
            pictureBox1.Image = Img;
            pictureBox1.Invalidate(); Pb.Value = Pb.Maximum;
            Status.Text = "Завершено";
        }

        private void Meta_Data(Image img)
        {
            ImageInfo newIInfo = new ImageInfo(img);

            foreach (KeyValuePair<PropertyTagId, KeyValuePair<
                PropertyTagType, Object>>
                property in newIInfo.imageProps)
            {
                if (property.Key == PropertyTagId.ExifFocalLength)
                {
                    focal = Convert.ToDouble(property.Value.ToString().Substring(11, property.Value.ToString().Length - 12));
                }
                if (property.Key == PropertyTagId.GpsLatitude)
                {
                    lat = Convert.ToDouble(property.Value.ToString().Substring(11, property.Value.ToString().Length-12));
                }
                if (property.Key == PropertyTagId.GpsLongitude)
                {
                    lon = Convert.ToDouble(property.Value.ToString().Substring(11, property.Value.ToString().Length - 12));
                }
                if (property.Key == PropertyTagId.DateTime)
                {
                    string DATA = property.Value.ToString().Substring(8, property.Value.ToString().Length - 9);
                    data = new DateTime(Convert.ToInt32(DATA.Substring(0, 4)), Convert.ToInt32(DATA.Substring(5, 2)),Convert.ToInt32(DATA.Substring(8, 2)));

                }

            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Info = listBox2.SelectedItem.ToString();
            string[] words = Info.Split('\t');
            Star star = new Query().QueryStar(words[1]);
            label1.Text = "Информация о звезде: \n Имя звезды : " + words[1] +
                " \n X = " + words[2] + "\n Y = " + words[3];
            if (star != null)
            {
                label1.Text += "\n Имя созвездия: " + star.Constellation.Name;
            }
            else
                MessageBox.Show("Дополнительная информация не найдена");
        }


    }
}
