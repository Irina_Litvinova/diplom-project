﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace diplom_work
{
    public partial class Filters : Form
    {
        public float RA, DE;
        public int Sharp=0, Method=0, Profil=0;
        public double focal = 12.96, azimut = 30;
        public Filters(double Focal)
        {
            focal = Focal;
            InitializeComponent();
            button3.DialogResult = DialogResult.OK;
            button4.DialogResult = DialogResult.Cancel;
            
        }

        private void Filters_Load(object sender, EventArgs e)
        {
            textBox1.Text = focal.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
                Sharp=0;
            if (radioButton2.Checked)
                Sharp=1;
            if (radioButton3.Checked)
                Sharp=2;
            if (radioButton10.Checked)
                Sharp = 4;
            if (radioButton4.Checked)
                Profil = 0;
            if (radioButton5.Checked)
                Profil = 1;
            if (radioButton6.Checked)
                Method = 0;
            if (radioButton7.Checked)
                Method = 1;
            if (radioButton8.Checked)
                Method = 2;
            focal = Convert.ToDouble(textBox1.Text);
            azimut = Convert.ToDouble(textBox2.Text);
            RA = Convert.ToSingle(textBox3.Text);
            DE = Convert.ToSingle(textBox4.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
