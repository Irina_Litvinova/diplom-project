﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace diplom_work
{
    public class SpacePoint
    {
        public double ksi;
        public double nu;
        public SpacePoint(int id, string catID, double ra, double de, float mag)
        {
            this.ID = id;
            this.CatID = catID;
            this.RA = ra;         //прямое восхождение звезды
            this.DE = de;         //склонение
            this.Magnitude = mag; //звездная величина
        }

        public SpacePoint(double ra, double de, float mag)
        {
            this.RA = ra;
            this.DE = de;
            this.Magnitude = mag;
        }

          
        public int ID;
        public readonly string CatID;
        public readonly double RA;
        public readonly double DE;
        public readonly float Magnitude;
        

        public void Calculate(double a, double b)
        {
            ksi = Math.Cos(DE) * Math.Sin(RA - a);
            double rab = Math.Sin(DE) * (Math.Sin(b) + Math.Cos(DE) * Math.Cos(b) * Math.Cos(RA - a)/Math.Sin(DE));
            ksi = ksi / rab;
            nu = Math.Sin(DE) * Math.Cos(b) - Math.Cos(DE) * Math.Sin(b) * Math.Cos(RA - a);
            nu = nu / rab;
        }
        

    }
}
